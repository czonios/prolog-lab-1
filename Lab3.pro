
%-----------------------------------------------------------------------------------------

%-- ASKHSH 1

%-- pur(I,C,N) :- pur(I,X,Y), purchase(I,A,X,Y), item(X,Z,G,P).
%-- price(Weight, Price, price/weight).
price(G,P,N) :- N is P/G.

lessprice(P1,P2) :- P1<P2.

%-- afterdiscount(Code, Discount%, Number of products, price after discount).
afterdiscount(C,D,N,T) :- item(C,X,G,P), T is (P-(P*(D/100)))*N.

p1(I,J) :- purchase(I,A,C,N), purchase(J,A,D,E).

p2(X,Y) :- purchase(I,A,C1,N), item(C1,X,G1,P1),
 purchase(J,A,C2,E), item(C2,Y,G2,P2).

p3(C) :- item(C,X,G,P), item(C2,X,G2,P3),
price(G,P,P1), price(G2,P3,P2), lessprice(P2,P1).

p4(I,T) :- purchase(I,A,C,N), item(C,X,G,P), discount(A,D),
 afterdiscount(C,D,N,T).

/*

%-----------------------------------------------------------------------------------------

%-- ASKHSH 2

path(X,Y) :- fail.

biconnected(X,Y) :- fail.

meetpoint(S1,D1,S2,D2,X) :- fail.






%-----------------------------------------------------------------------------------------

%-- ASKHSH 3

zeta(K,N,X,Y,G) :- fail.










%-----------------------------------------------------------------------------------------

%-- ASKHSH 4

divide(X,s(Y),s(D)) :- fail.








*/

%-----------------------------------------------------------------------------------------
%-----------------------------------------------------------------------------------------
%-----------------------------------------------------------------------------------------

%-- MHN TROPOPOIHSETE TO PARAKATW TMHMA KWDIKA


item(n3001,coffee,100,1.25).
item(n3002,coffee,200,2.40).
item(n3205,sugar,500,1.80).
item(d1105,milk,500,0.65).
item(d1110,milk,1000,1.20).
item(k2105,bread,500,0.90).
item(k2110,bread,1000,1.80).
item(k2120,bread,2000,3.60).
item(z1005,water,500,0.35).
item(z1010,water,1000,0.80).
item(n3201,tea,100,1.48).

purchase(inv001,'Mickey Mouse',n3002,3).
purchase(inv002,'Mickey Mouse',n3205,1).
purchase(inv008,'Mickey Mouse',d1110,2).
purchase(inv004,'Donald Duck',d1105,15).
purchase(inv005,'Donald Duck',k2120,50).
purchase(inv003,'Lucky Luke',z1005,3).
purchase(inv006,'Cocco Bill',n3002,2).
purchase(inv007,'Lucky Luke',z1005,7).

discount('Mickey Mouse', 10).
discount('Donald Duck',25).
discount('Lucky Luke',35).
discount('Cocco Bill',0).
discount('Woody Woodpecker',5).

source(a).
source(b).
source(c).

destination(x).
destination(y).
destination(z).

link(a,d).
link(a,e).
link(b,d).
link(b,f).
link(c,f).
link(c,g).
link(c,q).
link(d,h).
link(d,i).
link(e,j).
link(f,k).
link(f,w).
link(g,l).
link(h,i).
link(i,p).
link(i,x).
link(j,x).
link(j,y).
link(j,z).
link(k,y).
link(l,m).
link(m,n).
link(n,o).
link(o,i).
link(p,y).
link(q,r).
link(r,s).
link(r,u).
link(s,t).
link(t,u).
link(u,x).
link(v,z).
link(w,v).
link(w,z).
